#ntfs2fat
Zrobic dump
Convert files from NTFS partition to FAT32
"Jeśli mam katalog główny teksta
Wieksze niz 4k mniejsze od 4gb
nazwa i rozmiar
Ukryty tu i tu(uprawnienia)
Lael dysku nie

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [What mean mount][1]
* [How looks like FAT32][2]
* [Let's talk about NTFS...][3]
* [More about NTFS][4]
* [Quick Start with FAT32][5]
* [Getopts example][6]
[1]: https://unix.stackexchange.com/questions/3192/what-is-meant-by-mounting-a-device-in-linux
[2]: https://social.technet.microsoft.com/wiki/contents/articles/6771.the-fat-file-system.aspx
[3]: https://wiki.osdev.org/NTFS
[4]: http://www.ntfs.com/ntfs-mft.htm
[5]: https://www.pjrc.com/tech/8051/ide/fat32.html
[6]: https://github.com/karelzak/util-linux/blob/master/sys-utils/mount.c
