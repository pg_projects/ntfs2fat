#pragma once
#pragma pack(push, 1)

namespace FAT
{
	struct FatBPB
	{
		Byte JMP[3];
		Qword oem;
		Word bytesPerSector;
		Byte sectorsPerCluster;
		Word reservedSectors;
		Byte fatCopies;
		Word rootDirEntries;
		Word numSectors;
		Byte mediaType; // ?
		Word sectorsPerFat; // Fat16Only, not used in fat32
		Word sectorsPerTrack;
		Word numberOfHeads;
		Dword hiddenSectors;
		Dword sectorBig; //This field is set if there are more than 65535 sectors in the volume, resulting in a value which does not fit in the Number of Sectors entry at 0x16.
	//}__attribute__ ((__packed__));
	};

	struct FatEBP
	{
		Dword sectorPerFat;
		Word flags;
		Word fatVersion;
		Dword rootDirectoryClusterNumber;
		Word fsInfoSector;
		Word backupBootSectorNumber;
		Byte Reserved[12]; // set to 0
		Byte driveNumber;
		Byte flagsWindows;
		Byte signature;
		Dword volumeID;
		Byte volumeLabel[11];
		Qword systemString; // = "FAT32"
		Byte bootCode[420];
		Word bootPartitionSignature;
		//}__attribute__ ((__packed__));
	};

	enum FAT_FILE_FLAGS
	{
		ReadOnly = 0x01,
		Hidden = 0x02,
		System = 0x04,
		VolumeID = 0x08,
		Directory = 0x10,
		Archive = 0x20,
		LFN = 0x0f,
		Deleted = 0xe5,
		E5 = 0x05,
		Subdirectory = 0x2e
	};

	struct FatDirectory
	{
		Byte shortName[11];
		FAT_FILE_FLAGS attributes;
		/*  01 tylko do odczytu
		02 plik ukryty 04 plik systemowy '
		08 etykieta dysku
		10 katalog
		20 znacznik archwizacji
		0f dluga nazwa
		e5 plik skasowany
		05 pierwszym znakiem jest e5h
		2e opis podkatalogu; je�li nast�pny bajt zawiera tak�e kod 2EH (tj.
	'..'), to analizowana pozycja opisuje katalog nadrz�dny w
	stosunku do bie��cego � w tym przypadku pole 'Nr pocz�tkowej
	pozycji ...' wskazuje na numer pozycji w FAT, zawieraj�cej
	katalog nadrz�dny; zero oznacza, �e katalogiem nadrz�dnym
	jest katalog g��wny
		*/
		Byte reserved;
		Byte time10ms;
		Word fileCreationTime;
		Word fileCreationDate;
		Word lastAccessTime;
		Word fat32Start;
		Word lastModifiedTime;
		Word lastModifiedDate;
		Word fat32End;
		Dword fileSize;
		//}__attribute__ ((__packed__));
	};

	struct LongFileName
	{
		Byte fileNameEntry;
		Word first5Word[5];
		Byte attrConst; // 0x0F zawsze!
		Byte LongEntryType; // 0 for name
		Byte checksum;
		Word next6Word[6];
		Word zero;
		Word final2Word[2];
	};
}

#pragma pack(pop)